

module.exports = function Circle (radius){
    this.radius = radius;
    this.getLength = function(radius){
        return (2*Math.PI*this.radius);
    }
    this.getSquare = function(radius){
        return (Math.PI*this.radius**2)
    }
};

